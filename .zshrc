for file in ~/.zshrc.d/*; do
  source "$file"
done
source ~/.aliases

eval $(dircolors ~/.dircolors)

export KEYTIMEOUT=1

# dh_make
export DEBEMAIL="robin@mccorkell.me.uk"
export DEBFULLNAME="Robin McCorkell"

export TILLER_NAMESPACE=gitlab-managed-apps

export NOCOVERAGE=1

typeset -gxT PYTHONPATH pythonpath :
export GOPATH=$HOME/go
export SBT_OPTS="-Xmx4G -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled"

# Disable user autocomplete because it is slowwww
zstyle ':completion:*' users

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export PATH="$HOME/.poetry/bin:$PATH"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
