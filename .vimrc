set nocompatible
filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set tabstop=4
set shiftwidth=4
set nobackup
set nowb
set number
set nowrap
set smartcase
set timeoutlen=1000
set ttimeoutlen=10
set formatoptions+=r

set relativenumber

syntax enable
set laststatus=2

set hlsearch
nmap <silent> <Leader>/ :nohlsearch<CR>

cmap w!! w !sudo tee % >/dev/null

set undofile
set undodir=~/.vimundo/
