fish_vi_key_bindings
source ~/.aliases

set -x WECHALLUSER Xeno
if test -f ~/.wechalltoken
    set -x WECHALLTOKEN (cat ~/.wechalltoken)
end

if type -q pyenv
	pyenv init - | source
end
