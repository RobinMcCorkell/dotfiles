set $mod Mod4

input "type:keyboard" xkb_layout "us"
input "type:keyboard" xkb_variant "colemak"
input "type:keyboard" xkb_options "caps:escape"

# QWERTY barcode scanners
input "1504:4608:Symbol_Technologies,_Inc,_2008_Symbol_Bar_Code_Scanner" xkb_variant ""

font pango:DejaVu Sans 10

default_border pixel 2
smart_borders on

output "*" background /usr/share/backgrounds/sway/Sway_Wallpaper_Blue_2048x1536.png fill

# Laptop screen
#output eDP-1 pos 960 2160
output eDP-1 pos 0 0 scale 1.2

# Monitor in Dover
output "Unknown 2476WM F49GCBA010456" pos 960 1080

# WFH desk
output "Dell Inc. DELL U2720Q BYJY123" pos -980 -2160

input type:touchpad tap enabled

focus_follows_mouse no

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+q kill

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
#bindsym $mod+h split h

# split in vertical orientation
#bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+Shift+f fullscreen
bindsym $mod+F11 fullscreen

# change container layout (stacked, tabbed, toggle split)
# bindsym $mod+Shift+s layout stacking
# bindsym $mod+Shift+w layout tabbed
# bindsym $mod+Shift+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+Shift+a focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

bindsym $mod+Shift+bracketleft move workspace to output left
bindsym $mod+Shift+bracketright move workspace to output right
bindsym $mod+Shift+equal move workspace to output up
bindsym $mod+Shift+hyphen move workspace to output down

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

bindsym $mod+Shift+c reload
bindsym $mod+Shift+r restart
bindsym $mod+Delete exec loginctl lock-session
bindsym --locked $mod+Shift+Delete exec "killall shaderlock; loginctl lock-session"

set $mode_system System | (e) exit; (s) suspend; (h) hibernate; (r) reboot; (p) poweroff
mode "$mode_system" {
        bindsym e exit
        bindsym s exec systemctl suspend, mode "default"
        bindsym h exec systemctl hibernate, mode "default"
        bindsym r exec systemctl reboot, mode "default"
        bindsym p exec systemctl poweroff -i, mode "default"

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+Shift+Delete mode "$mode_system"

# Application shortcut keys
bindsym $mod+w exec gtk-launch google-chrome
bindsym $mod+t exec gtk-launch Alacritty
bindsym $mod+e exec gtk-launch code

bindsym $mod+d exec wofi --show=drun --insensitive

# Multimedia keys
bindsym --locked XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym --locked XF86AudioMicMute exec pactl set-source-mute @DEFAULT_SOURCE@ toggle

bindsym --locked $mod+F1 exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked $mod+F2 exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked $mod+F3 exec pactl set-sink-volume @DEFAULT_SINK@ +5%

bindsym --locked XF86AudioPlay exec playerctl -p spotify play
bindsym --locked XF86AudioPause exec playerctl -p spotify pause
bindsym --locked XF86AudioNext exec playerctl -p spotify next
bindsym --locked XF86AudioPrev exec playerctl -p spotify previous

bindsym --locked $mod+Shift+p exec playerctl -p spotify play-pause
bindsym --locked $mod+Shift+period exec playerctl -p spotify next
bindsym --locked $mod+Shift+comma exec playerctl -p spotify previous
bindsym $mod+Shift+slash exec gtk-launch com.spotify.Client
bindsym $mod+Shift+v exec gtk-launch pavucontrol

bindsym --locked XF86MonBrightnessDown exec light -T 0.5
bindsym --locked XF86MonBrightnessUp exec light -T 2

bindsym Print exec slurp | grim -g - - | wl-copy
bindsym Shift+Print exec slurp | grim -g - /tmp/screenshot.png

bindsym --locked XF86Keyboard exec ~/.config/sway/keyboard.sh
bindsym --locked $mod+F12 exec ~/.config/sway/keyboard.sh

bar swaybar_command waybar
exec blueman-applet
exec nm-applet --indicator
exec "systemctl --user import-environment; systemctl --user try-restart ssh-agent.service gpg-agent.service"
exec swayidle -w lock shaderlock.daemon before-sleep shaderlock.daemon
exec lxpolkit
exec mako
